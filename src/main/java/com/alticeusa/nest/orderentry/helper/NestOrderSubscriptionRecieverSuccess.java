package com.alticeusa.nest.orderentry.helper;

public class NestOrderSubscriptionRecieverSuccess {

	String name;
	String partnerSubscriptionId;
	String partnerCustomerId;
	String displayName;
	String productCode;
	String startTime;
	String status;
	String associationCode;
	String associationUrl;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPartnerSubscriptionId() {
		return partnerSubscriptionId;
	}
	public void setPartnerSubscriptionId(String partnerSubscriptionId) {
		this.partnerSubscriptionId = partnerSubscriptionId;
	}
	public String getPartnerCustomerId() {
		return partnerCustomerId;
	}
	public void setPartnerCustomerId(String partnerCustomerId) {
		this.partnerCustomerId = partnerCustomerId;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAssociationCode() {
		return associationCode;
	}
	public void setAssociationCode(String associationCode) {
		this.associationCode = associationCode;
	}
	public String getAssociationUrl() {
		return associationUrl;
	}
	public void setAssociationUrl(String associationUrl) {
		this.associationUrl = associationUrl;
	}
}
