package com.alticeusa.nest.orderentry.helper;

import com.alticeusa.nest.orderentry.data.Error;
public class NestOrderSubscriptionRecieverError {

	Error error;

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}
}
