package com.alticeusa.nest.orderentry.queue;

import java.io.IOException;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.nest.orderentry.data.NestOrderEquipmentPojo;
import com.alticeusa.nest.orderentry.helper.NestOrderEquipmentSenderHelper;
import com.alticeusa.nest.orderentry.metrics.logging.LogFormatter;
import com.alticeusa.nest.orderentry.utils.Constant;
import com.alticeusa.nest.orderentry.utils.PropertiesReader;
import com.alticeusa.nest.orderentry.utils.Utils;

@WebListener
public class NestOrderEquipmentReciever implements ServletContextListener,MessageListener {

	private static final Logger logger = LogManager.getLogger(NestOrderEquipmentReciever.class);
	
	static PropertiesReader prop = PropertiesReader.getInstance();
	Connection connection = null;
	ActiveMQConnectionFactory factory;
	Queue queue;
	MessageConsumer consumer = null;
	Session session = null;
	Destination destination = null;
	Context ctx = null;
	
	@Override
	public void onMessage(Message message) {
		logger.info(">>>>> In Equipment Queue Reciever onMessage for message: "+message.toString());
		String recieveMsg = null;
		try {
			recieveMsg = ((TextMessage)message).getText();
			NestOrderEquipmentPojo nestOrderEquipmentPojo = (NestOrderEquipmentPojo) Utils.convertJsonToNestOrderEquipmentPojo(recieveMsg);
			logger.info(">>> onMessage() ::: EquipmentRecieverQueuemessage = {}", LogFormatter.produceJSON(nestOrderEquipmentPojo, Constant.PRETTY_PRINT));
			logger.debug("Sending Equipment Message to Nest");
			NestOrderEquipmentSenderHelper.sendToNest(nestOrderEquipmentPojo,recieveMsg);
		} catch (JMSException | IOException e) {
			logger.error("Exception occured while reading message from the equipment queue", e);
			e.printStackTrace();
		}
		logger.info("<<<<< Out Equipment Queue Reciever onMessage for message: "+message.toString());
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		try {
			if (consumer != null) {
				consumer.close();
			}
			if (session != null) {
				session.close();
			}
			if (connection != null) {
				connection.close();
			}
		}catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.info(">>>>> In Equipment reciever Queue contextInitialized");
		Properties props = new Properties();
		props.setProperty(Context.INITIAL_CONTEXT_FACTORY, Constant.NEST_ORDERENTRY_SEND_CONTEXT_FACTORY);
		props.setProperty(Context.PROVIDER_URL, Constant.NEST_ORDERENTRY_SEND_URL);
		try {
			ctx = new InitialContext(props);
			factory = (ActiveMQConnectionFactory) ctx.lookup(Constant.CONNECTION_FACTORY);
			connection = factory.createConnection();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			queue = session.createQueue(Constant.NEST_ORDER_EQUIP_QUEUE_NAME);
			consumer = session.createConsumer(queue);
		    consumer.setMessageListener(this);
		    connection.start();
		} catch (NamingException | JMSException e) {
			logger.error("Exception occured while initiallizing queue connection", e);
			e.printStackTrace();
		}
		logger.info("<<<<< Out Equipment reciever Queue contextInitialized");
	}

	
}
