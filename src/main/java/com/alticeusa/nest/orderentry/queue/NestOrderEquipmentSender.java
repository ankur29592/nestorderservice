package com.alticeusa.nest.orderentry.queue;

import java.util.Properties;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.nest.orderentry.dao.NestOrderServiceDao;
import com.alticeusa.nest.orderentry.utils.Constant;

public class NestOrderEquipmentSender {

	private static final Logger logger = LogManager.getLogger(NestOrderEquipmentSender.class);
	
	public static NestOrderEquipmentSender instance = null;
	public static NestOrderEquipmentSender getInstance() throws Exception  {
		if (instance == null)
		{
			instance = new NestOrderEquipmentSender();
		}//if
		return instance;
	}
	
	Connection connection = null;
	ActiveMQConnectionFactory factory;
	Queue queue;
	MessageProducer producer = null;
	Session session = null;
	Destination destination = null;
	Context ctx = null;
	
	public void contextDestroyed() {
		try {
			if (producer != null) {
				producer.close();
			}
			if (session != null) {
				session.close();
			}
			if (connection != null) {
				connection.close();
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

	public void initializeContext() throws NamingException, JMSException {
			logger.info(">>>>> In Equipment Queue initializeContext");
			Properties props = new Properties();
			props.setProperty(Context.INITIAL_CONTEXT_FACTORY, Constant.NEST_ORDERENTRY_SEND_CONTEXT_FACTORY);
			props.setProperty(Context.PROVIDER_URL, Constant.NEST_ORDERENTRY_SEND_URL);
			ctx = new InitialContext(props);
			factory = (ActiveMQConnectionFactory) ctx.lookup(Constant.CONNECTION_FACTORY);
			connection = factory.createConnection();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			queue = session.createQueue(Constant.NEST_ORDER_EQUIP_QUEUE_NAME);
			producer = session.createProducer(queue);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			logger.info("<<<<< Out Equipment Queue initializeContext");
	}
	
	public void sendMsgToQueue(String queueMsg,int orderId,int sendRetry) {
		logger.info(">>>>> In Equipment Queue sendMsgToQueue for orderId: "+orderId + "queueMsg: " + queueMsg);
		
		try {
			initializeContext();
			Message jsonMessage = session.createTextMessage(queueMsg);
			logger.debug("Sending Message to Equipment queue");
			sendToQueue(jsonMessage,sendRetry,queueMsg,orderId);
		} catch (NamingException | JMSException e) {
			if(sendRetry > 0){
				logger.info("Retrying to send Message into Equipment queue");
				sendMsgToQueue(queueMsg,orderId,sendRetry-1);
			} else {
				logger.debug("Unable to send Message into Equipment queue ,calling dao updateOrderEquipmentError() for OrderId: " + orderId);
				NestOrderServiceDao nestOrderServiceDao = new NestOrderServiceDao();
				nestOrderServiceDao.updateOrderEquipmentError(orderId, Constant.NEST_ERROR_THRESHOLD_VALUE,"Error while try to publish the message into Equipment Queue",queueMsg,Constant.ERROR,Constant.NEST_DEFAULT_ERROR_STATUS);
			}

		} finally{
			contextDestroyed();
		}
		logger.info("<<<<< Out Equipment Queue sendMsgToQueue for orderId: "+orderId + "queueMsg" + queueMsg);
	}
	
	private void sendToQueue(Message jsonMessage,int sendRetry,String queueMsg,int orderId) {
		try{
			producer.send(jsonMessage);
			logger.debug("Message sent to Equipment queue");
		}catch(JMSException e){
			logger.error("Exception occured while sending the message in to equipment queue:", e);
					}
	}
}
