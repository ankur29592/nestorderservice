package com.alticeusa.nest.orderentry.utils;

import java.util.Properties;

public class NestOrderServiceProperties {

	public Properties prop = null;
	public static NestOrderServiceProperties singleton = null;
	
	public NestOrderServiceProperties() {
		loadProperties();
	}
	
	public static NestOrderServiceProperties getInstance()
	{
		if (singleton == null)
		{
			singleton = new NestOrderServiceProperties();
		}//if
		return singleton;
	}//getInstance
	
	public String getValue(String key) 
	{
		return prop.getProperty(key);
	}// getValue
	public Properties loadProperties()
	{
		prop = PropertiesReader.getInstance().getParams();
		return prop;
	}
}
