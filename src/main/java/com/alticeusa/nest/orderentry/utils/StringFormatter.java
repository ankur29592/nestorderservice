package com.alticeusa.nest.orderentry.utils;

import java.util.ArrayList;

public final class StringFormatter {

	public static boolean checkNullString(String... inputArray) {
		for (String inputString : inputArray) 
		{
			if(nullOrBlank(inputString))
			{
				return true;
			}
		}
		return false;
	}
	
	//new validation code
	/*public static String checkNullStrings(String... inputArray) {
		StringBuffer errormsg = new StringBuffer();
		errormsg.append("");
		for (String inputString : inputArray) 
		{
			if(nullOrBlank(inputString))
			{
				errormsg.append(inputString + " is missing ");
				return errormsg.toString();
			}
		}
		return errormsg.toString();
	}*/
	
	public static boolean checkNullArraylist(ArrayList<?> inputArraylist) {
		if(inputArraylist == null || inputArraylist.isEmpty()) {
			return true;
		}
		return false;
	}
	
	public static boolean checkNumeric(String field) {
		if(field.matches("{0-9]+"))
			return true;
		return false;
	}
	
	public static boolean checkDecimal(String field) {
		if(field.matches("{0-9]+[.][0-9]+"))
			return true;
		return false;
	}
	
	public static boolean nullOrBlank(String inputString)
	{
		return ( inputString == null ||
				( inputString != null && inputString.trim().equalsIgnoreCase(""))|| 
				(inputString != null && inputString.trim().length() == 0)||
				( inputString != null && inputString.trim().equalsIgnoreCase("null")));
	}//nullOrBlank
	
	public static boolean toBoolean(String inputString)
    {
		return (inputString == null || (inputString != null && inputString.trim().equalsIgnoreCase(""))
				|| (inputString != null && inputString.trim().length() == 0)
				|| (inputString != null && inputString.trim().equalsIgnoreCase("null")));
    }
}
