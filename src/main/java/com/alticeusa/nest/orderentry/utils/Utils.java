package com.alticeusa.nest.orderentry.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.alticeusa.nest.orderentry.data.Equipment;
import com.alticeusa.nest.orderentry.data.NestOrderEquipmentPojo;
import com.alticeusa.nest.orderentry.data.NestOrderServiceRequest;
import com.alticeusa.nest.orderentry.data.NestOrderServiceResponse;
import com.alticeusa.nest.orderentry.data.NestOrderSubscriptionPojo;
import com.alticeusa.nest.orderentry.data.Subscription;
import com.alticeusa.nest.orderentry.helper.NestOrderEquipmentRecieverHelper;
import com.alticeusa.nest.orderentry.helper.NestOrderSubscriptionRecieverError;
import com.alticeusa.nest.orderentry.helper.NestOrderSubscriptionRecieverSuccess;
import com.alticeusa.nest.orderentry.metrics.logging.LogFormatter;
import com.alticeusa.nest.orderentry.metrics.logging.NestOrderEntryLoggerHelper;

public class Utils {

	private static final Logger logger = LogManager.getLogger(Utils.class);

	public static NestOrderServiceResponse sendError(NestOrderServiceResponse response, String errorCode,
			String errorMessage) {
		response.setErrorCode(errorCode);
		response.setErrorMessage(errorMessage);
		return response;
	}

	public static void writeLogForError(NestOrderServiceRequest nestOrderServiceRequest) {
		logger.info("inside writeLogForError() creating log file for Error response");
		try {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String fileDate = sdf.format(date) + "/";

			String directoryName = Constant.LOG_FILE_PATH + fileDate;
			if (!nestOrderServiceRequest.getAccountNumber().isEmpty()) {
				String fileName = nestOrderServiceRequest.getAccountNumber() + ".txt";
				ObjectMapper mapper = new ObjectMapper();
				String jsonLog;
				jsonLog = mapper.writeValueAsString(nestOrderServiceRequest);
				Utils.writeToLogFile(fileName, directoryName, jsonLog);
			}
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String convertJavaToXml(NestOrderServiceRequest nestOrderServiceRequest) throws JAXBException {
		StringWriter xmlString = new StringWriter();
		JAXBContext contextObj = JAXBContext.newInstance(NestOrderServiceRequest.class);
		Marshaller marshaller = contextObj.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(nestOrderServiceRequest, xmlString);
		String xml = xmlString.toString();
		return xml;
	}

	private static String checkEquipmentandSubscriptionLists(NestOrderServiceRequest nestOrderServiceRequest) {
		StringBuffer errormsg = new StringBuffer();
		errormsg.append("");
		if (StringFormatter.checkNullArraylist(nestOrderServiceRequest.getEquipments())
				&& StringFormatter.checkNullArraylist(nestOrderServiceRequest.getSubscriptions())) {
			return errormsg.append("Atleast one (equipment or subscription) is required").toString();
		} else if (!(StringFormatter.checkNullArraylist(nestOrderServiceRequest.getEquipments()))) {
			for (Equipment e : nestOrderServiceRequest.getEquipments()) {
				if (StringFormatter.checkNullString(e.getRateCode())) {
					errormsg.append("rateCode is missing for equipment");
				}
				/*
				 * if(StringFormatter.checkNullString(e.getCost())) {
				 * errormsg.append("cost is missing for equipment"); }
				 * if(StringFormatter.checkNullString(e.getTax())) {
				 * errormsg.append("tax is missing for equipment"); }
				 */
			}
		} else if (!(StringFormatter.checkNullArraylist(nestOrderServiceRequest.getSubscriptions()))) {
			for (Subscription s : nestOrderServiceRequest.getSubscriptions()) {
				if (StringFormatter.checkNullString(s.getRateCode())) {
					errormsg.append("rateCode is missing for subscription");
				}
				/*
				 * if(StringFormatter.checkNullString(s.getDisplayName())) {
				 * errormsg.append("displayName is missing for subscription"); }
				 */
			}
		} else {
			return errormsg.toString();
		}
		return errormsg.toString();
	}

	public static String isSourceAppVaild(String sourceApp) {
		sourceApp = sourceApp.trim();
		String errorMsg = "";

		/*if (!("IDA".equalsIgnoreCase(sourceApp) || "ECOMMERCE".equalsIgnoreCase(sourceApp))) {
			errorMsg = "Invalid value for sourceApp ";
			return errorMsg;
		}*/
		return errorMsg;
	}

	public static String isFootPrintVaild(String footPrint) {
		footPrint = footPrint.trim();
		String errorMsg = "";

		/*
		 * if(!("OPT".equalsIgnoreCase(footPrint) ||
		 * "SDL".equalsIgnoreCase(footPrint))) { errorMsg =
		 * "Invalid value for footPrint "; return errorMsg; }
		 */
		return errorMsg;
	}

	public static String isCtypetVaild(String ctype) {
		ctype = ctype.trim();
		String errorMsg = "";

		/*
		 * if(!("RESIDENTIAL".equalsIgnoreCase(ctype) ||
		 * "BUSINESS".equalsIgnoreCase(ctype) ||
		 * "EMPLOYEE".equalsIgnoreCase(ctype) ||
		 * "NON_CUSTOMER".equalsIgnoreCase(ctype))) { errorMsg =
		 * "Invalid value for ctype "; return errorMsg; }
		 */
		return errorMsg;
	}

	public static String isLinkOrderIdVaild(String linkOrderId) {
		String errorMsg = "";
		if (StringFormatter.nullOrBlank(linkOrderId)) {
			return errorMsg;
		} else {
			linkOrderId = linkOrderId.trim();
			if (linkOrderId.length() > 30) {
				errorMsg = "Length too large for linkOrderId ";
			}
		}
		return errorMsg;
	}

	public static String isFirstNameVaild(String firstName) {
		firstName = firstName.trim();
		String errorMsg = "";
		if (firstName.length() > 15) {
			errorMsg = "Length too large for firstName ";
			return errorMsg;
		}
		return errorMsg;
	}

	public static String isLastNameVaild(String lastName) {
		lastName = lastName.trim();
		String errorMsg = "";
		if (lastName.length() > 25) {
			errorMsg = "Length too large for lastName ";
			return errorMsg;
		}
		return errorMsg;
	}

	public static String isAddress1Vaild(String address1) {
		address1 = address1.trim();
		String errorMsg = "";
		if (address1.length() > 200) {
			errorMsg = "Length too large for address1 ";
			return errorMsg;
		}
		return errorMsg;
	}

	public static String isAddress2Vaild(String address2) {
		String errorMsg = "";
		if (StringFormatter.nullOrBlank(address2)) {
			return errorMsg;
		} else {
			address2 = address2.trim();
			if (address2.length() > 200) {
				errorMsg = "Length too large for address2 ";
			}
		}
		return errorMsg;
	}

	public static String isCityVaild(String city) {
		city = city.trim();
		String errorMsg = "";
		if (city.length() > 30) {
			errorMsg = "Length too large for city ";
			return errorMsg;
		}
		return errorMsg;
	}

	public static String isStateVaild(String state) {
		state = state.trim();
		String errorMsg = "";
		if (state.length() > 2) {
			errorMsg = "Length too large for state ";
			return errorMsg;
		}
		return errorMsg;
	}

	public static String isCountryVaild(String country) {
		String errorMsg = "";
		if (StringFormatter.nullOrBlank(country)) {
			return errorMsg;
		} else {
			country = country.trim();
			if (country.length() > 2) {
				errorMsg = "Length too large for country ";
			}
		}
		return errorMsg;
	}

	public static String isZipCodeVaild(String zipcode) {
		zipcode = zipcode.trim();
		String errorMsg = "";
		if (zipcode.length() != 5) {
			errorMsg = "Invalid zipcode ";
			return errorMsg;
		}
		return errorMsg;
	}

	public static String isEmailVaild(String email) {
		email = email.trim();
		String errorMsg = "";
		if (email.length() > 150) {
			errorMsg = "Length too large for email ";
			return errorMsg;
		}
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		if (!matcher.matches()) {
			errorMsg = "Invalid email ";
		}
		return errorMsg;
	}

	public static String isPhoneVaild(String phone) {
		String errorMsg = "";
		phone = phone.replaceAll("[\\s\\-()]", "");
		phone = phone.replaceAll("\\s+", "");
		System.out.println(phone);
		if (phone.length() > 10 || !isNumber(phone)) {
			errorMsg = "Invalid phone number ";
			return errorMsg;
		}
		return errorMsg;
	}

	public static String isSignatureRequiredVaild(String signatureRequired) {
		String errorMsg = "";
		if (StringFormatter.nullOrBlank(signatureRequired)) {
			return errorMsg;
		} else {
			signatureRequired = signatureRequired.trim();
			if (signatureRequired.length() > 1) {
				errorMsg = "Length too large for signatureRequired ";
			}
		}
		return errorMsg;
	}

	public static String isSerialNumberVaild(String serialNumber) {
		String errorMsg = "";
		if (!StringFormatter.nullOrBlank(serialNumber))
		{
		serialNumber = serialNumber.trim();
		if (serialNumber.length() > 40) {
			errorMsg = "Length too large for serialNumber ";
			return errorMsg;
		}
		}
		return errorMsg;
	}

	public static String isQuantityVaild(String quantity) {
		String errorMsg = "";
		if (!StringFormatter.nullOrBlank(quantity)) {
		quantity = quantity.replaceAll("\\s+", "");
		if (!isNumber(quantity)) {
			errorMsg = "Quantity is not Number ";
			return errorMsg;
		}
		}
		return errorMsg;
	}

	public static String isCostVaild(String cost) {
		String errorMsg = "";
		if (!StringFormatter.nullOrBlank(cost)) {
			cost = cost.replaceAll("\\s+", "");

			if (cost.length() > 11 || !isNumberAndDecimal(cost)) {
				errorMsg = "Invalid cost value ";
			}
		}

		return errorMsg;
	}

	public static String isTaxVaild(String tax) {
		String errorMsg = "";
		if (!StringFormatter.nullOrBlank(tax)) {
			tax = tax.replaceAll("\\s+", "");
			if (tax.length() > 11 || !isNumberAndDecimal(tax)) {
				errorMsg = "Invalid tax value ";
			}
		}
		return errorMsg;
	}

	public static String isValidCost(NestOrderServiceRequest nestOrderServiceRequest) {
		String errorMsg = "";
		if (!StringFormatter.checkNullArraylist(nestOrderServiceRequest.getEquipments())) {
			for (Equipment e : nestOrderServiceRequest.getEquipments()) {
				if (!("".equals(isCostVaild(e.getCost())))) {
					errorMsg = isCostVaild(e.getCost());
					return errorMsg;
				}
			}
		}
		return errorMsg;
	}

	public static String isValidTax(NestOrderServiceRequest nestOrderServiceRequest) {
		String errorMsg = "";
		if (!StringFormatter.checkNullArraylist(nestOrderServiceRequest.getEquipments())) {
			for (Equipment e : nestOrderServiceRequest.getEquipments()) {
				if (!("".equals(isTaxVaild(e.getTax())))) {
					errorMsg = isTaxVaild(e.getTax());
					return errorMsg;
				}
			}
		}
		return errorMsg;
	}

	public static StringBuffer isDisplayNameVaild(String displayName, StringBuffer errorMsg) {
		if (!StringFormatter.nullOrBlank(displayName)) {
			displayName = displayName.trim();
		
		if (displayName.length() > 50) {
			errorMsg = errorMsg.append("Length too large for displayName ");
			return errorMsg;
		}
		}
		return errorMsg;
	}

	public static String isShippingCostVaild(String shippingCost) {
		String errorMsg = "";
		shippingCost = shippingCost.replaceAll("\\s+", "");
		if (shippingCost.length() > 9 || !isNumberAndDecimal(shippingCost)) {
			errorMsg = "Invalid shippingCost ";
			return errorMsg;
		}
		return errorMsg;
	}

	public static String isOrderCostVaild(String orderCost) {
		String errorMsg = "";
		orderCost = orderCost.replaceAll("\\s+", "");
		if (orderCost.length() > 9 || !isNumberAndDecimal(orderCost)) {
			errorMsg = "Invalid orderCost ";
			return errorMsg;
		}
		return errorMsg;
	}

	public static String isOrderTaxesVaild(String orderTaxes) {
		String errorMsg = "";
		orderTaxes = orderTaxes.replaceAll("\\s+", "");
		if (orderTaxes.length() > 9 || !isNumberAndDecimal(orderTaxes)) {
			errorMsg = "Invalid orderTaxes ";
			return errorMsg;
		}
		return errorMsg;
	}

	public static String isDescriptionVaild(String description) {
		String errorMsg = "";
		if (StringFormatter.nullOrBlank(description)) {
			return errorMsg;
		} else {
			description = description.trim();
			if (description.length() > 200) {
				errorMsg = "Length too large for description ";
			}
		}
		return errorMsg;
	}

	public static String isValidQuantity(NestOrderServiceRequest nestOrderServiceRequest) {
		String errorMsg = "";
		if (!StringFormatter.checkNullArraylist(nestOrderServiceRequest.getEquipments())) {
			for (Equipment e : nestOrderServiceRequest.getEquipments()) {
				if (!("".equals(isQuantityVaild(e.getQuantity())))) {
					errorMsg = "Quantity is not a number";
				}
			}
		}

		if (!StringFormatter.checkNullArraylist(nestOrderServiceRequest.getSubscriptions())) {
			for (Subscription s : nestOrderServiceRequest.getSubscriptions()) {
				if (!("".equals(isQuantityVaild(s.getQuantity())))) {
					errorMsg = "Quantity is not a number";
				}
			}
		}
		return errorMsg;
	}

	public static String convertJavaToJson(Object o) throws JsonGenerationException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(o);
		return jsonInString;
	}

	public static NestOrderEquipmentPojo convertJsonToNestOrderEquipmentPojo(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		NestOrderEquipmentPojo nestOrderEquipmentPojo = mapper.readValue(json, NestOrderEquipmentPojo.class);
		return nestOrderEquipmentPojo;
	}

	public static NestOrderEquipmentRecieverHelper convertJsonToNestOrderEquipmentRecieverHelper(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		NestOrderEquipmentRecieverHelper nestOrderEquipmentRecieverHelper = mapper.readValue(json,
				NestOrderEquipmentRecieverHelper.class);
		return nestOrderEquipmentRecieverHelper;
	}

	public static NestOrderSubscriptionPojo convertJsonToNestOrderSubscriptionPojo(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		NestOrderSubscriptionPojo nestOrderSubscriptionPojo = mapper.readValue(json, NestOrderSubscriptionPojo.class);
		return nestOrderSubscriptionPojo;
	}

	public static NestOrderSubscriptionRecieverSuccess convertJsonToNestOrderSubscriptionRecieverSuccess(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		NestOrderSubscriptionRecieverSuccess nestOrderSubscriptionRecieverSuccess = mapper.readValue(json,
				NestOrderSubscriptionRecieverSuccess.class);
		return nestOrderSubscriptionRecieverSuccess;
	}

	public static NestOrderSubscriptionRecieverError convertJsonToNestOrderSubscriptionRecieverError(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		NestOrderSubscriptionRecieverError nestOrderSubscriptionRecieverError = mapper.readValue(json,
				NestOrderSubscriptionRecieverError.class);
		return nestOrderSubscriptionRecieverError;
	}

	private static boolean isNumber(String input) {
		for (char c : input.toCharArray()) {
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}

	private static boolean isNumberAndDecimal(String input) {
		final String INPUT_PATTERN = "^\\d+(\\.\\d{1,2})?$";
		Pattern pattern = Pattern.compile(INPUT_PATTERN);
		Matcher matcher = pattern.matcher(input);
		if (!matcher.matches()) {
			return false;
		}
		return true;
	}

	public static String validateRequest(NestOrderServiceRequest nestOrderServiceRequest) {
		StringBuffer errorMsg = new StringBuffer();
		errorMsg.append("");
		if (!("".equals(isSourceAppVaild(nestOrderServiceRequest.getSourceApp())))) {
			errorMsg.append(isSourceAppVaild(nestOrderServiceRequest.getSourceApp()));
		}

		if (!("".equals(isFootPrintVaild(nestOrderServiceRequest.getFootprint())))) {
			errorMsg.append(isFootPrintVaild(nestOrderServiceRequest.getFootprint()));
		}

		if (!("".equals(isCtypetVaild(nestOrderServiceRequest.getCtype())))) {
			errorMsg.append(isCtypetVaild(nestOrderServiceRequest.getCtype()));
		}

		if (!("".equals(isLinkOrderIdVaild(nestOrderServiceRequest.getLinkOrderId())))) {
			errorMsg.append(isLinkOrderIdVaild(nestOrderServiceRequest.getLinkOrderId()));
		}

		if (!("".equals(isFirstNameVaild(nestOrderServiceRequest.getFirstName())))) {
			errorMsg.append(isFirstNameVaild(nestOrderServiceRequest.getFirstName()));
		}

		if (!("".equals(isLastNameVaild(nestOrderServiceRequest.getLastName())))) {
			errorMsg.append(isLastNameVaild(nestOrderServiceRequest.getLastName()));
		}

		if (!("".equals(isAddress1Vaild(nestOrderServiceRequest.getAddress1())))) {
			errorMsg.append(isAddress1Vaild(nestOrderServiceRequest.getAddress1()));
		}

		if (!("".equals(isAddress2Vaild(nestOrderServiceRequest.getAddress2())))) {
			errorMsg.append(isAddress2Vaild(nestOrderServiceRequest.getAddress2()));
		}

		if (!("".equals(isCityVaild(nestOrderServiceRequest.getCity())))) {
			errorMsg.append(isCityVaild(nestOrderServiceRequest.getCity()));
		}

		if (!("".equals(isStateVaild(nestOrderServiceRequest.getState())))) {
			errorMsg.append(isStateVaild(nestOrderServiceRequest.getState()));
		}

		if (!("".equals(isCountryVaild(nestOrderServiceRequest.getCountry())))) {
			errorMsg.append(isCountryVaild(nestOrderServiceRequest.getCountry()));
		}

		if (!("".equals(isZipCodeVaild(nestOrderServiceRequest.getZipCode())))) {
			errorMsg.append(isZipCodeVaild(nestOrderServiceRequest.getZipCode()));
		}

		if (!("".equals(isEmailVaild(nestOrderServiceRequest.getEmail())))) {
			errorMsg.append(isEmailVaild(nestOrderServiceRequest.getEmail()));
		}

		if (!("".equals(isPhoneVaild(nestOrderServiceRequest.getPhone())))) {
			errorMsg.append(isPhoneVaild(nestOrderServiceRequest.getPhone()));
		}

		if (!("".equals(isSignatureRequiredVaild(nestOrderServiceRequest.getSignatureRequired())))) {
			errorMsg.append(isSignatureRequiredVaild(nestOrderServiceRequest.getSignatureRequired()));
		}

		if (!("".equals(isShippingCostVaild(nestOrderServiceRequest.getShippingCost())))) {
			errorMsg.append(isShippingCostVaild(nestOrderServiceRequest.getShippingCost()));
		}

		if (!("".equals(isOrderCostVaild(nestOrderServiceRequest.getOrderCost())))) {
			errorMsg.append(isOrderCostVaild(nestOrderServiceRequest.getOrderCost()));
		}

		if (!("".equals(isOrderTaxesVaild(nestOrderServiceRequest.getOrderTaxes())))) {
			errorMsg.append(isOrderTaxesVaild(nestOrderServiceRequest.getOrderTaxes()));
		}

		if (!("".equals(isDescriptionVaild(nestOrderServiceRequest.getDescription())))) {
			errorMsg.append(isDescriptionVaild(nestOrderServiceRequest.getDescription()));
		}

		if (!("".equals(isValidCost(nestOrderServiceRequest)))) {
			errorMsg.append(isValidCost(nestOrderServiceRequest));
		}
		if (!("".equals(isValidTax(nestOrderServiceRequest)))) {
			errorMsg.append(isValidTax(nestOrderServiceRequest));
		}

		if (!("".equals(isValidQuantity(nestOrderServiceRequest)))) {
			errorMsg.append(isValidQuantity(nestOrderServiceRequest));
		}
		return errorMsg.toString();
	}

	// new validation
	public static String checkValidRequests(NestOrderServiceRequest nestOrderServiceRequest) {
		StringBuffer errorMsg = new StringBuffer();
		errorMsg.append("");
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getSourceApp())) {
			errorMsg.append("sourceApp is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getFootprint())) {
			errorMsg.append("footPrint is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getCtype())) {
			errorMsg.append("cType is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getAccountNumber())) {
			errorMsg.append("accountNumber is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getFirstName())) {
			errorMsg.append("firstName is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getLastName())) {
			errorMsg.append("lastName is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getAddress1())) {
			errorMsg.append("address1 is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getCity())) {
			errorMsg.append("city is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getState())) {
			errorMsg.append("state is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getZipCode())) {
			errorMsg.append("zipcode is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getEmail())) {
			errorMsg.append("email is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getPhone())) {
			errorMsg.append("phone is missing ");
		}
		if (!"".equals(checkEquipmentandSubscriptionLists(nestOrderServiceRequest))) {
			errorMsg.append(checkEquipmentandSubscriptionLists(nestOrderServiceRequest));
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getOrderCost())) {
			errorMsg.append("orderCost is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getOrderTaxes())) {
			errorMsg.append("orderTaxes is missing ");
		}
		if (StringFormatter.nullOrBlank(nestOrderServiceRequest.getFranchiseArea())) {
			errorMsg.append("franchiseArea is missing ");
		}

		return errorMsg.toString();
	}

	public static void writeToLogFile(String fileName, String directoryName, String msg) {
		logger.info("Writing to log file:");
		File directory = new File(directoryName);
		if (!directory.exists()) {
			directory.mkdir();
			// If you require it to make the entire directory path including
			// parents,
			// use directory.mkdirs(); here instead.
		}
		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			File file = new File(directoryName + "/" + fileName);
			fw = new FileWriter(file.getAbsoluteFile(), true);
			bw = new BufferedWriter(fw);
			bw.write(msg);
			bw.newLine();
			bw.close();

		} catch (IOException e) {
			logger.error("Error Occured while Writing to log file:", e);
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {

				ex.printStackTrace();
			}
		}
	}

	public static String getCurrentTimeStamp(String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		Date now = new Date();
		String formattedDateTime = dateFormat.format(now);
		return formattedDateTime;
	}

	public static void performMetricsLogging(NestOrderServiceRequest request, NestOrderServiceResponse response) {
		NestOrderEntryLoggerHelper metricsData = new NestOrderEntryLoggerHelper(request, response);
		logger.info(metricsData.toString());
	}

	public static void populateResponseWithValuesFromRequest(NestOrderServiceRequest request,
			NestOrderServiceResponse response) {
		response.setSourceApp(request.getSourceApp());
		response.setFootprint(request.getFootprint());
		response.setAccountNumber(request.getAccountNumber());
		if (request.getLinkOrderId() == null)
			response.setLinkOrderId("");
		else {
			response.setLinkOrderId(request.getLinkOrderId());
		}
		response.setErrorCode("");
		response.setErrorMessage("");
		response.setOrderNumber("");
		response.setSuccess("");
	}

	public static void performNecessaryLogging(NestOrderServiceRequest request, NestOrderServiceResponse response,
			String errorStackTrace, long startTime) {
		long endTime = System.currentTimeMillis();
		performApplicationLogging(request, response, startTime, endTime);
		performMetricsLogging(request, response);
	}

	public static void performApplicationLogging(NestOrderServiceRequest request, NestOrderServiceResponse response,
			long startTime, long endTime) {
		long elapsed = endTime - startTime;
		logger.info(" NestOrderService() [ Time Taken = { } milli-seconds] ::: response = {}", elapsed,
				LogFormatter.produceJSON(response, Constant.PRETTY_PRINT));
	}
}
