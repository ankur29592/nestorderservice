package com.alticeusa.nest.orderentry.service;

import java.io.StringWriter;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.alticeusa.nest.orderentry.BO.NestOrderServiceBO;
import com.alticeusa.nest.orderentry.data.NestOrderServiceRequest;
import com.alticeusa.nest.orderentry.data.NestOrderServiceResponse;
import com.alticeusa.nest.orderentry.utils.Constant;
import com.alticeusa.nest.orderentry.utils.NestOrderServiceProperties;
import com.alticeusa.nest.orderentry.utils.Utils;

@Path("/api/v1")
public class NestOrderService {

	private static final Logger logger = LogManager.getLogger(NestOrderService.class);
	static {
		loadLoggerContextAndProperties();
	}
	@POST
	@Path("/createOrder")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Consumes({ MediaType.APPLICATION_JSON })
	public String createOrder(NestOrderServiceRequest request , @DefaultValue("") @HeaderParam("Accept") String format) {
		NestOrderServiceResponse response = new NestOrderServiceResponse();
		logger.info("Invoked API : /nestOrderService/api/v1/createOrder");
		NestOrderServiceBO nestOrderServiceBO = new NestOrderServiceBO();
		response = (nestOrderServiceBO.orderEntry(request));
		String finalString = "";
		if(format.equalsIgnoreCase("application/xml") && format!= "")
		{
			try{
				logger.debug("Converting response to XML");
				StringWriter xmlString = new StringWriter();
				JAXBContext contextObj = JAXBContext.newInstance(NestOrderServiceResponse.class);  
			    Marshaller marshaller = contextObj.createMarshaller();  
			    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			    marshaller.marshal(response, xmlString);
			    String xmlInString = xmlString.toString();
			    finalString = xmlInString;
			    logger.debug("XML RESPONSE:"+finalString);
			
			}catch(Exception e){
				logger.error("Error while converting java response object to xml string ",e);
				}
			
		}
		else {
			
			try{
			finalString = Utils.convertJavaToJson(response);
		}catch(Exception e){
			logger.error("Error while converting java response object to json string ",e);
			}
		return finalString;
		}
		return finalString;
	}
	
	
	private static void loadAppVersionToLoggerContext() {
		ThreadContext.put("VERSION", Constant.API_VERSION);
	}
	
	private static void loadLoggerContextAndProperties() {
		loadAppVersionToLoggerContext();
		NestOrderServiceProperties.getInstance().loadProperties();
		logger.info(Constant.API_NAME + " Properties loaded for Version: " + Constant.API_VERSION);
	}
}
