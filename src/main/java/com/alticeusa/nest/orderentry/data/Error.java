package com.alticeusa.nest.orderentry.data;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;

public class Error {

	int code;
	String message;
	String status;
	@JsonProperty("details")
	ArrayList<NestSubscriptionError> details;
	@JsonProperty("errors")
	ArrayList<NestSubscriptionError> errors;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<NestSubscriptionError> getDetails() {
		return details;
	}
	public void setDetails(ArrayList<NestSubscriptionError> nestSubscriptionErrors) {
		this.details = nestSubscriptionErrors;
	}
	public ArrayList<NestSubscriptionError> getErrors() {
		return errors;
	}
	public void setErrors(ArrayList<NestSubscriptionError> errors) {
		this.errors = errors;
	}
	
}
