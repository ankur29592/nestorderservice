package com.alticeusa.nest.orderentry.data;

public class NestSubscriptionFieldViolation {

	private String field;
	private String description;
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
