package com.alticeusa.nest.orderentry.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class NestOrderSubscriptionList {

	

	ArrayList<NestOrderSubscriptionPojo> nestOrderSubscriptionPojoList;
	public NestOrderSubscriptionList() {
	}

	public NestOrderSubscriptionList(ResultSet rs) throws SQLException {
		nestOrderSubscriptionPojoList = new ArrayList<NestOrderSubscriptionPojo>();
		while(rs.next()){
			NestOrderSubscriptionPojo nestOrderSubscriptionPojo = new NestOrderSubscriptionPojo();
			nestOrderSubscriptionPojo.setPartnerSubscriptionId(rs.getString("ID_ORDER_SUBSCRIPTION"));
			nestOrderSubscriptionPojo.setPartnerCustomerId(rs.getString("ID_ORDER"));
			nestOrderSubscriptionPojo.setDisplayName(rs.getString("DISPLAY_NAME"));
			nestOrderSubscriptionPojo.setProductCode(rs.getString("PRODUCT_CODE"));
			nestOrderSubscriptionPojoList.add(nestOrderSubscriptionPojo);
		}
	}
	
	public ArrayList<NestOrderSubscriptionPojo> getNestOrderSubscriptionPojoList() {
		return nestOrderSubscriptionPojoList;
	}

	public void setNestOrderSubscriptionPojoList(ArrayList<NestOrderSubscriptionPojo> nestOrderSubscriptionPojoList) {
		this.nestOrderSubscriptionPojoList = nestOrderSubscriptionPojoList;
	}
	
}
