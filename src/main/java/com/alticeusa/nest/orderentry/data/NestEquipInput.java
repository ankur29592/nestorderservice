package com.alticeusa.nest.orderentry.data;

import java.util.ArrayList;

public class NestEquipInput {

	ArrayList<NestEquipmentOrderDetail> nestEquipmentOrderDetail;
	
	public NestEquipInput(ArrayList<NestEquipmentOrderDetail> nestEquipmentOrderDetail) {
		this.nestEquipmentOrderDetail = nestEquipmentOrderDetail;
	}
	public ArrayList<NestEquipmentOrderDetail> getOrders() {
		return nestEquipmentOrderDetail;
	}
	public void setOrders(ArrayList<NestEquipmentOrderDetail> nestEquipmentOrderDetail) {
		this.nestEquipmentOrderDetail = nestEquipmentOrderDetail;
	}
}
