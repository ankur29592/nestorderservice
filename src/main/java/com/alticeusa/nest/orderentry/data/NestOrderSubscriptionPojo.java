package com.alticeusa.nest.orderentry.data;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NestOrderSubscriptionPojo {
	private String partnerSubscriptionId;
	private String partnerCustomerId;
	private String productCode;
	private String displayName;
	
	
	public NestOrderSubscriptionPojo() {
	}
	
	
	public NestOrderSubscriptionPojo(ResultSet rs) throws SQLException {
		while(rs.next()) {
			this.partnerSubscriptionId = rs.getString("ID_ORDER_SUBSCRIPTION");
			this.partnerCustomerId = rs.getString("ID_ORDER");
			this.productCode = rs.getString("PRODUCT_CODE");
			this.displayName = rs.getString("DISPLAY_NAME");
		}
	}
	
	public String getPartnerSubscriptionId() {
		return partnerSubscriptionId;
	}
	public void setPartnerSubscriptionId(String partnerSubscriptionId) {
		this.partnerSubscriptionId = partnerSubscriptionId;
	}
	public String getPartnerCustomerId() {
		return partnerCustomerId;
	}
	public void setPartnerCustomerId(String partnerCustomerId) {
		this.partnerCustomerId = partnerCustomerId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
