package com.alticeusa.nest.orderentry.data;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;

public class ValidationResult {

	@JsonProperty("message")
	private String message;
	@JsonProperty("status")
	private String status;
	@JsonProperty("entries")
	private ArrayList<Object> entries;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<Object> getEntries() {
		return entries;
	}
	public void setEntries(ArrayList<Object> entries) {
		this.entries = entries;
	}
}
