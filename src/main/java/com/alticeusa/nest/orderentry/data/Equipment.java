package com.alticeusa.nest.orderentry.data;

public class Equipment {

	String rateCode;
	String serialNumber;
	String quantity = "1";
	String cost;
	String tax;
	
	public String getRateCode() {
		return rateCode;
	}
	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getQuantity() {
		return quantity;
	}
}
