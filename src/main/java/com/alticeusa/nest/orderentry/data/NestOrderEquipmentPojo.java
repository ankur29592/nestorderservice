package com.alticeusa.nest.orderentry.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NestOrderEquipmentPojo {
	private static final Logger logger = LogManager.getLogger(NestOrderEquipmentPojo.class);
	
	int orderId;
	ArrayList<NestEquipmentOrderDetail> orders;


	public NestOrderEquipmentPojo() {
	}

	public NestOrderEquipmentPojo(NestOrderServiceRequest nestOrderServiceRequest, ResultSet rs, int id_order) throws SQLException {
		this.orderId = id_order;
		orders = new ArrayList<NestEquipmentOrderDetail>();
		while(rs.next()){
			NestEquipmentOrderDetail order = new NestEquipmentOrderDetail();
			order.setSKU(rs.getString("SKU"));
			order.setQUANTITY(rs.getString("QUANTITY"));
			order.setAddress1(nestOrderServiceRequest.getAddress1().toUpperCase());
			order.setAddress2(nestOrderServiceRequest.getAddress2().toUpperCase());
			order.setCity(nestOrderServiceRequest.getCity());
			order.setCountry(nestOrderServiceRequest.getCountry());
			order.setEmail(nestOrderServiceRequest.getEmail());
			order.setFirstName(nestOrderServiceRequest.getFirstName().toUpperCase());
			order.setLANGUAGE_PREFRENCE("en-us");
			order.setLastName(nestOrderServiceRequest.getLastName().toUpperCase());
			order.setORDER_DATE(rs.getString("ORDER_DATE"));
			order.setORDER_NUMBER(String.valueOf(id_order));
			order.setPhone(nestOrderServiceRequest.getPhone());
			order.setPOSTAL_CODE(nestOrderServiceRequest.getZipCode());
			order.setSignatureRequired(nestOrderServiceRequest.getSignatureRequired());
			order.setState(nestOrderServiceRequest.getState());
			order.setOPTION1(rs.getString("legal_name"));
			orders.add(order);
		}
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public ArrayList<NestEquipmentOrderDetail> getOrders() {
		return orders;
	}

	public void setOrders(ArrayList<NestEquipmentOrderDetail> orders) {
		this.orders = orders;
	}

}
