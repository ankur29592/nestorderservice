package com.alticeusa.nest.orderentry.cems;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.ResultSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.nest.orderentry.utils.Constant;

public class CemsEmailHandler {
	private static final Logger logger = LogManager.getLogger(CemsEmailHandler.class);

	public void sendEmail(EmailData emailData,ResultSet rs_equip)throws Exception {
		HTMLGenerator htmlGenerator = new HTMLGenerator();
		logger.debug(">>> sendEmail");
		try {
			StringBuffer emailBody = new StringBuffer();
			emailBody = htmlGenerator.htmlGenerator(emailData,rs_equip);
			String soapRequest = prepareCEMSSoapRequest(emailData, emailBody);
			String newLine = System.getProperty("line.separator");
			logger.info("the request sent to CEMS is: " + newLine + soapRequest);
			sendRequest(soapRequest);
		} catch (Exception e) {
			logger.error("Error occured while sending email." + e);
		}
	}

	private String prepareCEMSSoapRequest(EmailData emailData, StringBuffer emailBody) {
		String soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n<soapenv:Header/>\n<soapenv:Body> \n"
				+ prepareCEMSSoapBody(emailData, emailBody) + "</soapenv:Body> \n" + "</soapenv:Envelope> \n";
		return soapRequest;
	}

	public String prepareCEMSSoapBody(EmailData emailData, StringBuffer emailBody) {
		

		StringBuffer stdRequestBuffer = new StringBuffer("<cems:EmailConfig xmlns:cems=\"http://nwm.cablevision.com/cems\"> \n");
		stdRequestBuffer.append("<cems:FROMName/>\n");
		stdRequestBuffer.append("<cems:FROMAddress/>\n");
		stdRequestBuffer.append("<cems:TOAddress>" + emailData.getRecepientAdd() + "</cems:TOAddress> \n");
		stdRequestBuffer.append("<cems:Body>" + emailBody +"</cems:Body>");					
		stdRequestBuffer.append("<cems:EmailServer>" + Constant.CEMS_EMAIL_SERVER  + "</cems:EmailServer> \n");
		stdRequestBuffer.append("<cems:TemplateId>" + Constant.CEMS_TEMPLATE + "</cems:TemplateId> \n");
		String endRequest = "</cems:EmailConfig> \n";
		String soapBody = stdRequestBuffer.toString() + endRequest;
		return soapBody;
	}

	private void sendRequest(String soapRequest) throws IOException {
		try{
		// Create the connection where we're going to send the file.
		logger.info(">>> In sendRequest():");
		logger.info(">>> Sending the SOAP request to: "+Constant.CEMS_URL);
		logger.info("Getting connection");
		URL url = new URL(Constant.CEMS_URL);
		URLConnection connection = url.openConnection();

		HttpURLConnection httpConn = (HttpURLConnection) connection;
		logger.info("HTTP Connection made");
		String authorization = new sun.misc.BASE64Encoder().encode((Constant.USER_NAME + ":" + Constant.PASSWORD).getBytes());
		// Set the appropriate HTTP parameters.
		httpConn.setRequestProperty("Authorization", "Basic " + authorization);
		httpConn.setRequestProperty("Content-Length", String.valueOf(soapRequest.length()));
		httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
		httpConn.setRequestProperty("SOAPAction", "/CEMS Process/Email Service");
		httpConn.setRequestProperty("User-Agent", "Jakarta Commons-HttpClient/3.1");
		httpConn.setRequestProperty("Accept-Encoding", "gzip,deflate");
		httpConn.setRequestMethod("POST");
		httpConn.setDoOutput(true);
		httpConn.setDoInput(true);
		logger.info("sending XML request to connection");
		// Everything's set up; send the XML 
		OutputStream out = httpConn.getOutputStream();
		logger.info("Connected");
		 Writer wout = new OutputStreamWriter(out);
		 wout.write(soapRequest);
		 wout.flush();
		logger.info("XML request sent to CEMS URL");
	
		 logger.info("Getting Response from CEMS");
	       
		 // Read the response.
	        httpConn.connect();
	        logger.info("http connection code :"+ httpConn.getResponseCode());
	        logger.info("http connection status :"+ httpConn.getResponseMessage());
	      
	        if(httpConn.getResponseCode() == 200 && httpConn.getResponseCode() == 202){
	        	 logger.info("Success response from CEMS :");
	        	  InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
	        	  BufferedReader in = new BufferedReader(isr);
			        String inputLine;
			        logger.info("Response from CEMS is : ");
			        while ((inputLine = in.readLine()) != null)
			        	 logger.info(inputLine+"\n");		      			    
			        in.close();
			        isr.close();
	        }
	        else{
	        	logger.info("Error response from CEMS :");
	        	InputStreamReader error =  new InputStreamReader(httpConn.getErrorStream());
	        	BufferedReader in = new BufferedReader(error);
		        String inputLine;
		        logger.info("Response from CEMS is : ");
		        while ((inputLine = in.readLine()) != null)
		        	 logger.info(inputLine+"\n");		    
		        in.close();
	        	error.close();
	        }
	      
		 wout.close();
		}
		catch(Exception ex)
		{
			logger.error("Exception occured while calling CEMS in sendRequest() ",ex);
			throw ex;
		}
	}

	
}
