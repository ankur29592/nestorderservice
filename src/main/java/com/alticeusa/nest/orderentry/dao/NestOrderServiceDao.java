package com.alticeusa.nest.orderentry.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.nest.orderentry.cems.EmailSender;
import com.alticeusa.nest.orderentry.data.Item;
import com.alticeusa.nest.orderentry.data.NestOrderEquipmentPojo;
import com.alticeusa.nest.orderentry.data.NestOrderServiceDaoResponse;
import com.alticeusa.nest.orderentry.data.NestOrderServiceRequest;
import com.alticeusa.nest.orderentry.data.NestOrderServiceResponse;
import com.alticeusa.nest.orderentry.data.NestOrderSubscriptionList;
import com.alticeusa.nest.orderentry.data.NestOrderSubscriptionPojo;
import com.alticeusa.nest.orderentry.helper.NestOrderEquipmentRecieverHelper;
import com.alticeusa.nest.orderentry.helper.NestOrderSubscriptionRecieverError;
import com.alticeusa.nest.orderentry.helper.NestOrderSubscriptionRecieverSuccess;
import com.alticeusa.nest.orderentry.utils.Constant;
import com.alticeusa.nest.orderentry.utils.Utils;

import oracle.jdbc.OracleTypes;

public class NestOrderServiceDao {

	private static final Logger logger = LogManager.getLogger(NestOrderServiceDao.class);
	
	static Connection con = null;
	static CallableStatement  csmt = null;

	public NestOrderServiceDaoResponse createOrder(NestOrderServiceRequest nestOrderServiceRequest, String orderInfoXml, NestOrderServiceResponse nestOrderServiceResponse) throws SQLException {
		logger.info(">>>>> In DAO createOrder for orderInfo Xml: "+orderInfoXml);
		NestOrderServiceDaoResponse nestOrderServiceDaoResponse = null;
		NestOrderEquipmentPojo nestOrderEquipmentPojo = null;
		NestOrderSubscriptionList nestOrderSubscriptionList = null;
		NestOrderServiceConncectionDao nestOrderServiceConncectionDao;
		ResultSet rs_eqiup = null;
		ResultSet rs_subs = null;
		try {
			//logger.info(">>> createOrder() ::: request = {}", LogFormatter.produceJSON(nestOrderServiceRequest, Constant.PRETTY_PRINT));
			nestOrderServiceConncectionDao = NestOrderServiceConncectionDao.getInstance();
			logger.debug("Getting DB connection.");
			con = nestOrderServiceConncectionDao.getDBConnection();
			if(con == null){
				logger.debug("DB  not connected.");
				nestOrderServiceResponse = Utils.sendError(nestOrderServiceResponse, Constant.ERR_500, Constant.ERR_500_message);
				nestOrderServiceDaoResponse = new NestOrderServiceDaoResponse(nestOrderEquipmentPojo, nestOrderSubscriptionList, nestOrderServiceResponse);
			
				return nestOrderServiceDaoResponse;
			}
			logger.debug("DB connected.");
			String createorder = "{call google_nest.create_order(?,?,?,?,?,?,?,?)}";
			csmt = con.prepareCall(createorder);
			csmt.setString(1,nestOrderServiceRequest.getAccountNumber());
			csmt.setString(2,nestOrderServiceRequest.getCtype());
			csmt.setString(3,orderInfoXml);
			csmt.registerOutParameter(4, OracleTypes.INTEGER);
			csmt.registerOutParameter(5, OracleTypes.CURSOR);
			csmt.registerOutParameter(6, OracleTypes.CURSOR);
			csmt.registerOutParameter(7, OracleTypes.INTEGER);
			csmt.registerOutParameter(8, OracleTypes.VARCHAR);
			csmt.executeUpdate();
			
			logger.info("Executed : " + createorder);
			logger.info("csmt.getInt(4) orderId: " + csmt.getInt(4));
			logger.info("csmt.getObject(5) EquipmentList: ");
			logger.info("csmt.getObject(6) SubscriptionList: ");
			logger.info("csmt.getInt(7) errorCode: " + csmt.getInt(7));
			logger.info("csmt.getString(8) errorMsg: " + csmt.getString(8));
			
			int orderId = csmt.getInt(4);

			int DbErrorCode = csmt.getInt(7);
			
			String errorMsg = csmt.getString(8);
			String errorCode="";
			System.out.println(errorMsg);
			
				if(DbErrorCode !=0 ) {
				errorCode = Constant.ERR_404;
				nestOrderServiceResponse.setErrorCode(errorCode);
				nestOrderServiceResponse.setErrorMessage(errorMsg);
				nestOrderServiceDaoResponse = new NestOrderServiceDaoResponse(nestOrderEquipmentPojo, nestOrderSubscriptionList, nestOrderServiceResponse);
			}else {
				
				if ((ResultSet)csmt.getObject(5) != null)
					rs_eqiup = (ResultSet) csmt.getObject(5);
				
				if (csmt.getObject(6) != null)
					rs_subs = (ResultSet) csmt.getObject(6);
				errorCode = "";
				errorMsg = "";
				if(rs_eqiup!=null)
					nestOrderEquipmentPojo = new NestOrderEquipmentPojo(nestOrderServiceRequest, rs_eqiup, orderId);
				if(rs_subs!=null)
				nestOrderSubscriptionList = new NestOrderSubscriptionList(rs_subs);
				nestOrderServiceResponse.setErrorCode(errorCode);
				nestOrderServiceResponse.setErrorMessage(errorMsg);
				nestOrderServiceResponse.setOrderNumber(String.valueOf(orderId));
				nestOrderServiceDaoResponse = new NestOrderServiceDaoResponse(nestOrderEquipmentPojo, nestOrderSubscriptionList, nestOrderServiceResponse);
				
			}
		} catch (Exception e) {
			logger.error("Exception occured while calling SP create_order():", e);
			nestOrderServiceResponse = Utils.sendError(nestOrderServiceResponse, Constant.ERR_500, Constant.ERR_500_message);
			nestOrderServiceDaoResponse = new NestOrderServiceDaoResponse(nestOrderEquipmentPojo, nestOrderSubscriptionList, nestOrderServiceResponse);
		} finally {
			logger.info("In create order finally");
					if(con!= null ) {
						if(rs_eqiup != null )
						rs_eqiup.close();
						if(rs_subs != null )
						rs_subs.close();
						csmt.close();
						con.close();
					}
					
		}
		logger.info("<<<<< Out DAO createOrder for orderInfo Xml: "+orderInfoXml+"; NestOrderServiceDaoResponse from DB: "+nestOrderServiceDaoResponse);
		return nestOrderServiceDaoResponse;
	}
	

	public void updateOrderEquipmentInfo(NestOrderEquipmentRecieverHelper nestOrderEquipmentRecieverHelper,int orderId) {
		logger.info(">>>>> In DAO updateOrderEquipmentInfo for orderId: "+orderId);
		NestOrderServiceConncectionDao nestOrderServiceConncectionDao;
		try {
			nestOrderServiceConncectionDao = NestOrderServiceConncectionDao.getInstance();
			logger.debug("Getting DB connection.");
			con = nestOrderServiceConncectionDao.getDBConnection();
			logger.debug("DB connected.");
			String updateOrderEquipmentInfo = "{call google_nest.update_order_equip_create_info(?,?,?,?,?,?)}";
			csmt = con.prepareCall(updateOrderEquipmentInfo);
			for(Item item : nestOrderEquipmentRecieverHelper.getItems()) {
				csmt.setInt(1,orderId);
				csmt.setString(2,item.getStatus());
				csmt.setString(3,item.getOrderNumber());
				csmt.registerOutParameter(4, OracleTypes.INTEGER);
				csmt.registerOutParameter(5, OracleTypes.INTEGER);
				csmt.registerOutParameter(6, OracleTypes.VARCHAR);
				csmt.executeUpdate();
				
				logger.info("Executed : " + updateOrderEquipmentInfo);
			}
		} catch (Exception e) {
			logger.error("Exception occured while calling SP update_order_equip_create_info():", e);
		}  finally {
			if(con!= null)
				try {
					csmt.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		logger.info("<<<<< Out DAO updateOrderEquipmentInfo for orderId: "+orderId);
	}
	
	public void updateOrderSummaryInfo(NestOrderEquipmentRecieverHelper nestOrderEquipmentRecieverHelper,int orderId) {
		logger.info(">>>>> In DAO updateOrderSummaryInfo for orderId: "+orderId);
		NestOrderServiceConncectionDao nestOrderServiceConncectionDao;
		try {
			nestOrderServiceConncectionDao = NestOrderServiceConncectionDao.getInstance();
			logger.debug("Getting DB connection.");
			con = nestOrderServiceConncectionDao.getDBConnection();
			logger.debug("DB connected.");
			String updateOrderSummaryInfo = "{call google_nest.update_order_summary_info(?,?,?,?,?,?,?,?)}";
			csmt = con.prepareCall(updateOrderSummaryInfo);
			for(Item item : nestOrderEquipmentRecieverHelper.getItems()) {
				csmt.setInt(1,orderId);
				csmt.setString(2, Constant.ORDERED_STATUS);
				csmt.setInt(3,nestOrderEquipmentRecieverHelper.getTotalRecords());
				csmt.setString(4,nestOrderEquipmentRecieverHelper.getValidationResult().getStatus());
				csmt.setString(5,item.getStatus());
				csmt.registerOutParameter(6, OracleTypes.INTEGER);
				csmt.registerOutParameter(7, OracleTypes.INTEGER);
				csmt.registerOutParameter(8, OracleTypes.VARCHAR);
				csmt.executeUpdate();
				
				logger.info("Executed : " + updateOrderSummaryInfo);
			}
		} catch (Exception e) {
			logger.error("Exception occured while calling SP update_order_summary_info():", e);
		}  finally {
			if(con!= null)
				try {
					csmt.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		logger.info("<<<<< Out DAO updateOrderSummaryInfo for orderId: "+orderId);
	}
	
	public void updateOrderEquipmentError(int orderId,int errorCount, String errorMessage,String jsonLog,String status,String neststatus) {
		logger.info(">>>>> In DAO updateOrderEquipmentError for orderId: "+orderId);
		NestOrderServiceConncectionDao nestOrderServiceConncectionDao;
		try{
			nestOrderServiceConncectionDao = NestOrderServiceConncectionDao.getInstance();
			logger.debug("Getting DB connection.");
			con = nestOrderServiceConncectionDao.getDBConnection();
			logger.debug("DB connected.");
			String updateOrderEquipmentError = "{call google_nest.update_order_and_equip_error(?,?,?,?,?,?,?,?)}";
			csmt = con.prepareCall(updateOrderEquipmentError);
			csmt.setInt(1,orderId);
			csmt.setString(2,status);
			csmt.setInt(3,errorCount);
			csmt.setString(4,errorMessage);
			csmt.setString(5,neststatus);
			csmt.registerOutParameter(6, OracleTypes.INTEGER);
			csmt.registerOutParameter(7, OracleTypes.INTEGER);
			csmt.registerOutParameter(8, OracleTypes.VARCHAR);
			csmt.executeUpdate();
			
			logger.info("Executed : " + updateOrderEquipmentError);
			
		}catch (Exception e) {
			logger.error("Exception occured while calling SP update_order_and_equip_error():", e);
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMDD");
			String fileDate = sdf.format(date) + "/";
			 String directoryName = Constant.LOG_FILE_PATH + fileDate;
			String fileName =Constant.EQUIP_FILE_NAME + orderId  ;
			Utils.writeToLogFile(fileName,directoryName, jsonLog);
		} finally {
			if(con!= null)
				try {
					csmt.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		logger.info("<<<<< Out DAO updateOrderEquipmentError for orderId: "+orderId);	
	}
	
	public void updateOrderSubscriptionStatus(NestOrderSubscriptionRecieverSuccess nestOrderSubscriptionRecieverSuccess) throws SQLException {
		logger.info(">>>>> In DAO updateOrderSubscriptionStatus for orderId: "+nestOrderSubscriptionRecieverSuccess.getPartnerCustomerId()+" SubscriptionId: " + nestOrderSubscriptionRecieverSuccess.getPartnerSubscriptionId());	
		NestOrderServiceConncectionDao nestOrderServiceConncectionDao;
		try {
			nestOrderServiceConncectionDao = NestOrderServiceConncectionDao.getInstance();
			logger.debug("Getting DB connection.");
			con = nestOrderServiceConncectionDao.getDBConnection();
			logger.debug("DB connected.");
			String updateOrderSubscriptionStatus = "{call google_nest.update_subscription_status(?,?,?,?,?,?)}";
			csmt = con.prepareCall(updateOrderSubscriptionStatus);
			csmt.setInt(1,Integer.parseInt(nestOrderSubscriptionRecieverSuccess.getPartnerSubscriptionId()));
			csmt.setInt(2,Integer.parseInt(nestOrderSubscriptionRecieverSuccess.getPartnerCustomerId()));
			csmt.setString(3,nestOrderSubscriptionRecieverSuccess.getStatus());
			csmt.registerOutParameter(4, OracleTypes.INTEGER);
			csmt.registerOutParameter(5, OracleTypes.INTEGER);
			csmt.registerOutParameter(6, OracleTypes.VARCHAR);
			csmt.executeUpdate();
			
			logger.info("Executed : " + updateOrderSubscriptionStatus);
		} catch (Exception e) {
			logger.error("Exception occured while calling SP update_subscription_status():", e);
			System.out.println(e.getMessage());
		} finally {
			if(con!= null)
			con.close();
		}
		logger.info("<<<<< Out DAO updateOrderSubscriptionStatus for orderId: "+nestOrderSubscriptionRecieverSuccess.getPartnerCustomerId()+" SubscriptionId: " + nestOrderSubscriptionRecieverSuccess.getPartnerSubscriptionId());
	}
	
	
	public void updateOrderSubscriptionInfo(NestOrderSubscriptionRecieverSuccess nestOrderSubscriptionRecieverSuccess) throws SQLException {
		NestOrderServiceConncectionDao nestOrderServiceConncectionDao;
		String nestSubscriptionId = getNestSubscriptionId(nestOrderSubscriptionRecieverSuccess.getName());
		logger.info(">>>>> In DAO updateOrderSubscriptionInfo for orderId: "+nestOrderSubscriptionRecieverSuccess.getPartnerCustomerId()+" SubscriptionId: " + nestOrderSubscriptionRecieverSuccess.getPartnerSubscriptionId() + " NestSubscriptionId: " + nestSubscriptionId);
		try{
			nestOrderServiceConncectionDao = NestOrderServiceConncectionDao.getInstance();
			logger.debug("Getting DB connection.");
			con = nestOrderServiceConncectionDao.getDBConnection();
			logger.debug("DB connected.");
			String updateOrderSubscriptionInfo = "{call google_nest.update_order_subscription_info(?,?,?,?,?,?,?,?,?,?)}";
			csmt = con.prepareCall(updateOrderSubscriptionInfo);
			csmt.setInt(1,Integer.parseInt(nestOrderSubscriptionRecieverSuccess.getPartnerSubscriptionId()));
			csmt.setInt(2,Integer.parseInt(nestOrderSubscriptionRecieverSuccess.getPartnerCustomerId()));
			csmt.setInt(3,Integer.parseInt(nestSubscriptionId));
			csmt.setString(4,nestOrderSubscriptionRecieverSuccess.getStatus());
			csmt.setString(5,nestOrderSubscriptionRecieverSuccess.getStartTime());
			csmt.setString(6,nestOrderSubscriptionRecieverSuccess.getAssociationCode());
			csmt.setString(7,nestOrderSubscriptionRecieverSuccess.getAssociationUrl());
			csmt.registerOutParameter(8, OracleTypes.INTEGER);
			csmt.registerOutParameter(9, OracleTypes.INTEGER);
			csmt.registerOutParameter(10, OracleTypes.VARCHAR);
			csmt.executeUpdate();
			
			logger.info("Executed : " + updateOrderSubscriptionInfo);
		} catch (Exception e) {
			logger.error("Exception occured while calling SP update_order_subscription_info():", e);
			System.out.println(e.getMessage());
		} finally {
			if(con!= null)
			con.close();
		}
		logger.info("<<<<< Out DAO updateOrderSubscriptionInfo for orderId: "+nestOrderSubscriptionRecieverSuccess.getPartnerCustomerId()+" SubscriptionId: " + nestOrderSubscriptionRecieverSuccess.getPartnerSubscriptionId() + " NestSubscriptionId: " + nestSubscriptionId);
	}
	
	
	public void updateOrderNestSubscriptionError(NestOrderSubscriptionRecieverError nestOrderSubscriptionRecieverError,NestOrderSubscriptionPojo nestOrderSubscriptionPojo) throws SQLException {
		NestOrderServiceConncectionDao nestOrderServiceConncectionDao;
		logger.info(">>>>> In DAO updateOrderNestSubscriptionError for orderId: "+nestOrderSubscriptionPojo.getPartnerCustomerId()+" SubscriptionId: " + nestOrderSubscriptionPojo.getPartnerSubscriptionId());	
		try{
			nestOrderServiceConncectionDao = NestOrderServiceConncectionDao.getInstance();
			logger.debug("Getting DB connection.");
			con = nestOrderServiceConncectionDao.getDBConnection();
			logger.debug("DB connected.");
			String updateOrderSubscriptionError = "{call google_nest.update_nest_subscribe_error(?,?,?,?,?,?,?,?)}";
			csmt = con.prepareCall(updateOrderSubscriptionError);
			csmt.setInt(1,Integer.parseInt(nestOrderSubscriptionPojo.getPartnerCustomerId()));
			csmt.setInt(2,Integer.parseInt(nestOrderSubscriptionPojo.getPartnerSubscriptionId()));
			csmt.setString(3,nestOrderSubscriptionRecieverError.getError().getStatus());
			csmt.setInt(4,nestOrderSubscriptionRecieverError.getError().getCode());
			csmt.setString(5,nestOrderSubscriptionRecieverError.getError().getMessage());
			csmt.registerOutParameter(6, OracleTypes.INTEGER);
			csmt.registerOutParameter(7, OracleTypes.INTEGER);
			csmt.registerOutParameter(8, OracleTypes.VARCHAR);
			csmt.executeUpdate();
			logger.info("Executed : " + updateOrderSubscriptionError);
		} catch (Exception e) {
			logger.error("Exception occured while calling SP update_nest_subscribe_error():", e);
			System.out.println(e.getMessage());
		} finally {
			if(con!= null)
			con.close();
		}
		logger.info("<<<<< Out DAO updateOrderNestSubscriptionError for orderId: "+nestOrderSubscriptionPojo.getPartnerCustomerId()+" SubscriptionId: " + nestOrderSubscriptionPojo.getPartnerSubscriptionId());	
	}
	
	public void updateOrderSubscriptionError(String orderId,String subscriptionId,String status,int errorCount, String errorMessage,String jsonLog) {
		NestOrderServiceConncectionDao nestOrderServiceConncectionDao;
		logger.info(">>>>> In DAO updateOrderSubscriptionError for orderId: "+orderId+" SubscriptionId: " + subscriptionId);	
		try{
			nestOrderServiceConncectionDao = NestOrderServiceConncectionDao.getInstance();
			logger.debug("Getting DB connection.");
			con = nestOrderServiceConncectionDao.getDBConnection();
			logger.debug("DB connected.");
			String updateOrderSubscriptionError = "{call google_nest.upd_order_subscribe_error(?,?,?,?,?,?,?,?)}";
			csmt = con.prepareCall(updateOrderSubscriptionError);
			csmt.setInt(1,Integer.parseInt(orderId));
			csmt.setInt(2,Integer.parseInt(subscriptionId));
			csmt.setString(3,status);
			csmt.setInt(4,errorCount);
			csmt.setString(5,errorMessage);
			csmt.registerOutParameter(6, OracleTypes.INTEGER);
			csmt.registerOutParameter(7, OracleTypes.INTEGER);
			csmt.registerOutParameter(8, OracleTypes.VARCHAR);
			csmt.executeUpdate();
			logger.info("Executed : " + updateOrderSubscriptionError);
		} catch (Exception e) {
			logger.error("Exception occured while calling SP upd_order_subscribe_error():", e);
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String fileDate = sdf.format(date) + "/";
			String fileName =  Constant.SUBSCRIB_FILE_NAME +orderId;
			 String directoryName = Constant.LOG_FILE_PATH + fileDate;
			Utils.writeToLogFile(fileName,directoryName, jsonLog);
		} finally {
			if(con!= null)
				try {
					csmt.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		logger.info("<<<<< Out DAO updateOrderSubscriptionError for orderId: "+orderId+" SubscriptionId: " + subscriptionId);	
	}
	
	private String getNestSubscriptionId(String name) {
		String temp[] = name.split("/");
		return temp[temp.length-1];
		
	}
	/*public void sendShippingMail(int orderId) throws Exception{
		NestOrderServiceConncectionDao nestOrderServiceConncectionDao;
		ResultSet rs_eqiup = null;
		EmailSender emailSender = new EmailSender();
		logger.info(">>>>> In DAO sendShippingMail for orderId: "+orderId);	
		try {
		nestOrderServiceConncectionDao = NestOrderServiceConncectionDao.getInstance();
		
			
			con = nestOrderServiceConncectionDao.getDBConnection();
			String shippedEquipDetails = "call google_nest.SHIPPED_EQUIP_DETAILS(?,?,?,?,?)";	

			csmt = con.prepareCall(shippedEquipDetails);
			
			csmt.setInt(1, orderId);
			csmt.registerOutParameter(2, OracleTypes.INTEGER);
			csmt.registerOutParameter(3, OracleTypes.VARCHAR);
			csmt.registerOutParameter(4, OracleTypes.VARCHAR);
			csmt.registerOutParameter(5, OracleTypes.CURSOR);
			csmt.executeUpdate();
			logger.info("Executed : " + shippedEquipDetails);
			logger.info("csmt.getInt(2) po_code: " + csmt.getInt(2));
			logger.info("csmt.getString(3) po_message: "+ csmt.getString(3) );
			String email = csmt.getString(4);
			int po_code = csmt.getInt(2);
			if(po_code == 0){
				if ((ResultSet)csmt.getObject(5) != null)
					rs_eqiup = (ResultSet) csmt.getObject(5);
				if(rs_eqiup!=null)
					emailSender.send(rs_eqiup,email,orderId);
				}
			else{
				logger.error("Error while calling SHIPPED_EQUIP_DETAILS(?,?,?,?,?) OrderId not found or S");
			}

		} 
		catch ( SQLException se) {
			if (se.getMessage() != null)
				logger.error("SQLException while calling SHIPPED_EQUIP_DETAILS(?,?,?,?,?)",se);
			}catch(Exception e)
		{
			if (e.getMessage() != null)
				logger.error("Exception while calling SHIPPED_EQUIP_DETAILS(?,?,?,?,?)",e);
			throw e;
		}finally {
			
				try {
					csmt.close();
					if (con != null)
					con.close();
				} catch (SQLException e) {
					logger.error("Exception while closing connection", e);
				}
		}

	}*/
	public int[] checkSubscriptions(int orderId){
		NestOrderServiceConncectionDao nestOrderServiceConncectionDao;
		int[] count = new int[2];
		logger.info(">>>>> In DAO checkSubscriptions for orderId: "+orderId);	
		try {
		nestOrderServiceConncectionDao = NestOrderServiceConncectionDao.getInstance();
		
			
			con = nestOrderServiceConncectionDao.getDBConnection();
			String check_nest_order = "call google_nest.check_nest_order(?,?,?,?,?)";	

			csmt = con.prepareCall(check_nest_order);
			
			csmt.setInt(1, orderId);
			csmt.registerOutParameter(2, OracleTypes.INTEGER);
			csmt.registerOutParameter(3, OracleTypes.INTEGER);
			csmt.registerOutParameter(4, OracleTypes.INTEGER);
			csmt.registerOutParameter(5, OracleTypes.VARCHAR);
			
			
			csmt.executeUpdate();
			logger.info("Executed : " + check_nest_order);
			logger.info("csmt.getInt(2) equip_count : " + csmt.getInt(2));
			logger.info("csmt.getString(3) sub_count: "+ csmt.getInt(3) );
			logger.info("csmt.getInt(4) po_code: " + csmt.getInt(4));
			logger.info("csmt.getString(5) po_message: "+ csmt.getString(5) );
			int po_code = csmt.getInt(4);
			String po_message = csmt.getString(5);
			int equip_count = csmt.getInt(2);
			int sub_count = csmt.getInt(3);
			count[0]=equip_count;
			count[1]=sub_count;
			
//			if (po_code == 0){
//				logger.info("checkSubscriptions():: Received Response from Nest for all subscriptions for orderID",orderId);
//				return true;
//			}
//			
//			else{
//				if(po_message.equalsIgnoreCase("No Subscriprion Found")){
//					logger.info("checkSubscriptions()::No subscriptions for orderID",orderId);
//					return true;
//				}
//				else if(po_message.equalsIgnoreCase("Subscription not sent to nest ")){
//					logger.info("checkSubscriptions():: Response not received from Nest for all subscriptions for orderID",orderId);
//					return false;
//				}}
					
		}

		catch (Exception e) {
			
			if (e.getMessage() != null)
				logger.error("Exception while calling check_subscriptions(?,?,?)",e);
			
		} finally {
			
				try {
					csmt.close();
					if (con != null)
					con.close();
				} catch (SQLException e) {
					logger.error("Exception while closing connection", e);
				}
				
		}
		return count;
	}
	/*public void setActiveStatus(int orderId){
		NestOrderServiceConncectionDao nestOrderServiceConncectionDao;
	
		logger.info(">>>>> In DAO setActiveStatus for orderId: "+orderId);	
		try {
			nestOrderServiceConncectionDao = NestOrderServiceConncectionDao.getInstance();
			
			con = nestOrderServiceConncectionDao.getDBConnection();
			String check_nest_order = "call google_nest.set_status_active(?,?,?)";	

			csmt = con.prepareCall(check_nest_order);
			
			csmt.setInt(1, orderId);
			csmt.registerOutParameter(2, OracleTypes.INTEGER);
			csmt.registerOutParameter(3, OracleTypes.VARCHAR);				
			csmt.executeUpdate();
			logger.info("Executed : " + check_nest_order);
			logger.info("csmt.getInt(2) po_code: " + csmt.getInt(2));
			logger.info("csmt.getString(3) po_message: "+ csmt.getString(3) );
			int po_code = csmt.getInt(2);
			String po_message = csmt.getString(3);
			
			
			if (po_code == 0){
			logger.info("setActiveStatus():: Subscriptions status set to ACTIVE for orderId: ",orderId);
				
			}
			
			else{
				
					logger.error("setActiveStatus():: Subscriptions status not set to ACTIVE for orderId:",orderId,po_message);				
			
					
		}
		}
		catch (Exception e) {
			
			if (e.getMessage() != null)
				logger.error("Exception while calling setActiveStatus(?,?,?)",e);
			
		} finally {
			
				try {
					csmt.close();
					if (con != null)
					con.close();
				} catch (SQLException e) {
					logger.error("Exception while closing connection", e);
				}
				
		}
	
	}*/
	}

